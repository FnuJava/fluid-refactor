package com.paic.arch.jmsbroker;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JmsMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
    private static ActiveMqJmsMessageSupport ACTIVE_SUPPORT;
    private static String REMOTE_BROKER_URL;
    
    

    @BeforeClass
    public static void setup() throws Exception {
        ACTIVE_SUPPORT =new  ActiveMqJmsMessageSupport();
        REMOTE_BROKER_URL = ACTIVE_SUPPORT.getConBrokerUrl().connectToBroke().getBrokerUrl();
    }

    @AfterClass
    public static void teardown() throws Exception {
    	ACTIVE_SUPPORT.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        String receivedMessage =   ACTIVE_SUPPORT.bindToBrokerAtUrl(REMOTE_BROKER_URL)
                .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT).andThen().
                retrieveASingleMessageFromTheDestination(TEST_QUEUE);
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
    }


}