package com.paic.arch.jmsbroker;

/**
 * 自定义异常
 * @author Administrator
 *
 */
public class NoMessageReceivedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoMessageReceivedException(String reason) {
		super(reason);
	}
}