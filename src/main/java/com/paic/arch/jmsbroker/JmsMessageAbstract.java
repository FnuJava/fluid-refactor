package com.paic.arch.jmsbroker;

/**
 * JMS模板类
 * @author Administrator
 *
 */
public abstract class JmsMessageAbstract {
	
	/**
	 * 获取BROKER_URL
	 */
	public abstract JmsMessageAbstract getConBrokerUrl();
	
	
	/**
	 * 建立连接
	 */
	public abstract JmsMessageAbstract connectToBroke();
	
	/**
	 * 发送消息
	 * @param aDestinationName
	 * @param aMessageToSend
	 * @return
	 */
	public abstract JmsMessageAbstract sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend);
	
	/**
	 * 接收消息
	 * @param aDestinationName
	 * @return
	 */
	public abstract String retrieveASingleMessageFromTheDestination(String aDestinationName);
	
	/**
	 * 关闭连接
	 * @return 
	 */
	public abstract void stopTheRunningBroker();
	
	/**
	 * 设置BROKER_URL
	 * @param aBrokerUrl
	 * @return
	 */
	public abstract JmsMessageAbstract bindToBrokerAtUrl(String aBrokerUrl);
	
	public final JmsMessageAbstract andThen() {
	        return this;
	}
}
